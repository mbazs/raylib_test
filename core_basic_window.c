#include "raylib.h"

int main()
{
    int screenWidth = 800;
    int screenHeight = 600;

    InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");
    SetTargetFPS(60);
    Camera2D cam = {0};
    cam.target = (Vector2){0, 0};
    cam.offset = (Vector2){screenWidth / 2.0f, screenHeight / 2.0f};
    cam.rotation = 0.0f;
    cam.zoom = 1.0f;
    bool rot = true;
    float rot_delta = 0.0f;

    InitAudioDevice();
    Music music = LoadMusicStream("res/country.mp3");
    PlayMusicStream(music);
    SetMusicVolume(music, .7f);

    while (!WindowShouldClose()) // Detect window close button or ESC key
    {
        BeginDrawing();

        BeginMode2D(cam);
        ClearBackground(DARKGRAY);
        DrawText("Árvíztürö tükörfúrógép", screenWidth / 4.0f, screenHeight / 4.0f, 20, LIGHTGRAY);
        EndMode2D();

        EndDrawing();

        if (rot)
        {
            cam.rotation += .32f + rot_delta;
        }

        if (IsKeyDown(KEY_LEFT))
        {
            cam.target.x += 5;
        }
        else if (IsKeyDown(KEY_RIGHT))
        {
            cam.target.x -= 5;
        }
        else if (IsKeyDown(KEY_UP))
        {
            cam.target.y += 5;
        }
        else if (IsKeyDown(KEY_DOWN))
        {
            cam.target.y -= 5;
        }
        else if (IsKeyPressed(KEY_SPACE))
        {
            rot = !rot;
        }
        else if (IsKeyDown(KEY_KP_ADD))
        {
            rot_delta += .01f;
        }
        else if (IsKeyDown(KEY_KP_SUBTRACT))
        {
            rot_delta -= .01f;
        }

        cam.zoom += (float)GetMouseWheelMove() * .05f;

        UpdateMusicStream(music);
    }
    CloseWindow(); // Close window and OpenGL context
    CloseAudioDevice();
    return 0;
}
